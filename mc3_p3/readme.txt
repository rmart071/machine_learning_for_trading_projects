In order to run:
- Part 1
-- need util.py file
Just need to type in: python indicators.py

- Part 2
-- need util.py and indicators.py file
--- to check out of sample, replace default date values
Just need to type in python rule_based.py


- Part 3
-- need util.py, indicators.py, BagLearner.py, RTLearner.py, and rule_based.py files
--- to test out of sample, use variables: out_sample_start and out_sample_end
Just need to type in python ML_based.py

- Part 4
-- need util.py, indicators.py, BagLearner.py, RTLearner.py, rule_based.py, and analysis.py files
--- uncomment commented code Lines 379-386, 402, 406, 410, 414 in def  market_simulator. Als update the dates similar to part 3 and update dates in analysis.py to view baseline (prices)
Just need to type in python ML_based.py
Type in analysis.py to see baseline data, update dates to view out of sample
