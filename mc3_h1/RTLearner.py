"""
A FAKE Random Tree Learner.  (c) 2016 Tucker Balch
This is just a linear regression learner implemented named RTLearner so
it is used as a template or placeholder for use by testbest4.  You should
replace this code with your own RTLearner.
"""

import numpy as np

class RTLearner(object):

    def __init__(self, verbose = False, leaf_size = 1):
        self.leaf_size = int(leaf_size)
        pass # move along, these aren't the drones you're looking for

    class data(object):
        def __init__(self, dataX, dataY):
            self.x = dataX
            self.y = dataY

    def get_random_choice(self,data):
        return np.random.choice(data.shape[0], 2, replace=False)

    def get_random_feature(self,data):
        return np.random.choice(data.shape[1], 1,replace=False)


    def build_tree(self, data):
        if data.x.shape[0] <= self.leaf_size:
            if data.y.dtype is None:
                return
            if data.y.shape[0] > 0:
                return np.array([[-1, data.y.mean(), np.nan, np.nan]], dtype=float)

        if data.y.size > 0:
            if self.same_values(data.x):
                if data.y.dtype is None:
                    return np.array([[-1, -1, np.nan, np.nan]], dtype=float)
                return np.array([[-1, data.y.mean(),np.nan, np.nan]], dtype=float)
            if self.same_values(data.y):
                return np.array([[-1, data.y[0],np.nan, np.nan]], dtype=float)

        if data.y.size == 0:
            return np.array([[-1, 0, np.nan, np.nan]], dtype=float)

        random1, random2 = self.get_random_choice(data.x)
        i = self.get_random_feature(data.x)[0]

        split_val = (data.x[random1, i] + data.x[random2, i]) / 2

        data_left = self.data(data.x[data.x[:,i] <= split_val],
                              data.y[data.x[:,i] <= split_val])

        data_right = self.data(data.x[data.x[:,i] > split_val],
                               data.y[data.x[:,i] > split_val])

        left_tree = np.array(self.build_tree(data_left))
        right_tree = np.array(self.build_tree(data_right))
        root = np.array([i, split_val, 1, left_tree.shape[0] + 1])

        while len(left_tree.shape) == 0 or len(right_tree.shape) == 0:
            random1, random2 = self.get_random_choice(data.x)
            i = self.get_random_feature(data.x)[0]
            split_val = (data.x[random1, i] + data.x[random2, i]) / 2
            data_left = self.data(data.x[data.x[:, i] <= split_val],
                                  data.y[data.x[:, i] <= split_val])

            data_right = self.data(data.x[data.x[:, i] > split_val],
                                   data.y[data.x[:, i] > split_val])

            left_tree = np.array(self.build_tree(data_left))
            right_tree = np.array(self.build_tree(data_right))
            root = np.array([i, split_val, 1, left_tree.shape[0] + 1])

        root = np.reshape(root, (-1, 4))

        return np.vstack((root, left_tree, right_tree))

    def addEvidence(self,dataX,dataY):
        """
        @summary: Add training data to learner
        @param dataX: X values of data to add
        @param dataY: the Y training values
        """

        data_set = self.data(dataX, dataY)
        tree2 = self.build_tree(data_set)
        self.tree = tree2
        return self.tree

    def query(self,points):
        """
        @summary: Estimate a set of test points given the model we built.
        @param points: should be a numpy array with each row corresponding to a specific query.
        @returns the estimated values according to the saved model.
        """
        x = []

        leaf_value, split_value = 0, 0
        rows_test_data = points.shape[0]

        i = 0

        while i < rows_test_data:
            j = 0
            factor = self.tree[j][0]
            # if factor is -1 this means we've reached the leaf

            while factor != -1:
                factor = self.tree[j][0]
                value = points[i][factor]
                split_value = self.tree[j][1]

                if value <= split_value:
                    next_location = j + self.tree[j][2]
                else:
                    next_location = j + self.tree[j][3]

                j = next_location

            leaf_value = split_value
            x.append(leaf_value)
            i = i + 1
        print(x)

        return x

    def same_values(self, lst):
        if lst.size == 0:
            return True

        if lst is None:
            return True

        var = lst[1:] == lst[:-1]
        return np.all(var)


        # get the linear result
        # ret_val = (self.model_coefs[:-1] * points).sum(axis = 1) + self.model_coefs[-1]
        # add some random noise
	# ret_val = ret_val + 0.09 * np.random.normal(size = ret_val.shape[0])
	# return ret_val

if __name__=="__main__":
    print "get me a shrubbery"
