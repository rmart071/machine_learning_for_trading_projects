import numpy as np


class BagLearner(object):
    def __init__(self, learner, kwargs = {"leaf_size":1}, bags = 20, boost = False, verbose = False):

        self.kwargs =kwargs
        self.bags = bags
        self.boost = boost
        self.verbose = verbose

        learners = []
        kwargs = kwargs
        for i in range(0, bags):
            learners.append(learner(**kwargs))

        self.learner = learners

        pass

    def addEvidence(self, dataX, dataY):

        for i in range(0, len(self.learner)):
            self.learner[i].addEvidence(dataX, dataY)


    def query(self, pointsX):
        x = []

        for i in range(0, len(self.learner)):
            x.append(self.learner[i].query(pointsX))

        return x

