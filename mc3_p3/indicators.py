import pandas as pd
import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import gridspec
import numpy as np
import datetime as dt
from util import get_data, plot_data



def get_bollinger_bands(sma , rolling_std):
    top_band = sma + (2 * rolling_std)
    bottom_band = sma - (2 * rolling_std)

    return top_band, bottom_band


def create_simple_moving_average(df, lookback):
    return pd.rolling_mean(df, window=lookback, min_periods=lookback)


def create_rolling_std(df, lookback):
    return pd.rolling_std(df, window=lookback, min_periods=lookback)


def create_exponential_moving_average(df, lookback):
    return pd.ewma(df, span=lookback, min_periods=lookback)


def create_sma_price_ratio(sma, price_df):
    return price_df / sma

def normalize_dataframe(df):
    return df / df.ix[0]


def create_ema_chart(prices, sma, ema, macd):
    plt.rc('axes', grid=True)
    plt.rc('grid', color='0.75', linestyle='-', linewidth=0.5)

    textsize = 9
    left, width = 0.1, 0.8
    rect1 = [left, 0.4, width, 0.4]
    rect2 = [left, 0.2, width, 0.2]

    fig = plt.figure(facecolor='white')
    axescolor = '#f6f6f6'  # the axes background color

    ax2 = fig.add_axes(rect1, axisbg=axescolor)
    ax3 = fig.add_axes(rect2, axisbg=axescolor, sharex=ax2)

    ax2.plot(prices.index, prices, label='Prices')
    ax2.plot(prices.index, sma, label='SMA')
    ax2.plot(prices.index, ema, label='EMA')
    ax2.set_xlabel("Date")
    ax2.set_ylabel("Price")
    ax2.legend(loc='upper left')

    ax2.text(0.025, 0.95, 'Price/SMA/EMA', va='top',
             transform=ax2.transAxes, fontsize=textsize)

    ax3.plot(prices.index, macd, label='MACD')
    ax3.set_xlabel("Date")
    ax3.set_ylabel("Price")
    ax3.text(0.025, 0.95, 'MACD', va='top',
             transform=ax3.transAxes, fontsize=textsize)

    plt.savefig('images/ema_price.png')


def create_bb_chart(prices, rolling_std, upper_band, lower_band,  sma, bbp):
    plt.rc('axes', grid=True)
    plt.rc('grid', color='0.75', linestyle='-', linewidth=0.5)

    textsize = 9
    left, width = 0.1, 0.8
    rect1 = [left, 0.4, width, 0.4]
    rect2 = [left, 0.2, width, 0.2]

    fig = plt.figure(facecolor='white')
    axescolor = '#f6f6f6'  # the axes background color

    ax2 = fig.add_axes(rect1, axisbg=axescolor)
    ax3 = fig.add_axes(rect2, axisbg=axescolor, sharex=ax2)
    ax2.plot(prices.index, prices, label='Prices')
    ax2.plot(prices.index, sma, label='SMA')
    ax2.plot(prices.index, upper_band, label='Upper Band')
    ax2.plot(prices.index, lower_band, label='Lower Band')
    ax2.set_xlabel("Date")
    ax2.set_ylabel("Price")
    ax2.legend(loc='upper left')

    ax2.text(0.025, 0.95, 'Bollinger Bands', va='top',
             transform=ax2.transAxes, fontsize=textsize)

    ax3.plot(prices.index, bbp, label='B%')
    ax3.set_xlabel("Date")
    ax3.set_ylabel("Price")
    ax3.text(0.025, 0.95, 'B%', va='top',
             transform=ax3.transAxes, fontsize=textsize)
    plt.savefig('images/bollinger_band.png')



def create_sma_chart(sma, sma_rate, prices):
    plt.rc('axes', grid=True)
    plt.rc('grid', color='0.75', linestyle='-', linewidth=0.5)

    textsize = 9
    left, width = 0.1, 0.8
    rect1 = [left, 0.4, width, 0.4]
    rect2 = [left, 0.2, width, 0.2]

    fig = plt.figure(facecolor='white')
    axescolor = '#f6f6f6'  # the axes background color

    ax2 = fig.add_axes(rect1, axisbg=axescolor)
    ax3 = fig.add_axes(rect2, axisbg=axescolor, sharex=ax2)
    ax2.plot(prices.index, prices, label='Prices')
    ax2.plot(prices.index, sma, label='SMA')
    ax2.set_xlabel("Date")
    ax2.set_ylabel("Price")
    ax2.legend(loc='upper left')

    ax2.text(0.025, 0.95, 'SMA & PRICE', va='top',
             transform=ax2.transAxes, fontsize=textsize)

    ax3.plot(prices.index, sma_rate, label='Price/SMA')
    ax3.set_xlabel("Date")
    ax3.set_ylabel("Price")
    ax3.text(0.025, 0.95, 'Price/SMA', va='top',
             transform=ax3.transAxes, fontsize=textsize)
    plt.savefig('images/sma_price.png')


def create_macd(df, lookback1, lookback2):
    ema1 = create_exponential_moving_average(df, lookback1)
    ema2 = create_exponential_moving_average(df, lookback2)

    return ema2 - ema1


def create_charts(symbols=['IBM'], lookback=14, start_date='2006-01-01', end_date='2009-12-31'):
    dates = pd.date_range(start_date, end_date)
    prices = get_data(symbols, dates)

    del prices['SPY']


    ### Calculate SMA-14 over the entire period in a single step.
    sma = create_simple_moving_average(prices, lookback)
    sma_ratio = prices / sma
    create_sma_chart(sma, sma_ratio, prices)

    macd = create_macd(prices, 26, 12)


    ema = create_exponential_moving_average(prices, lookback)
    create_ema_chart(prices,sma,ema, macd)

    ### Calculate Bollinger Bands (14 day) over the entire period.
    rolling_std = create_rolling_std(prices, lookback)
    top_band, bottom_band = get_bollinger_bands(sma, rolling_std)

    bbp = (prices - bottom_band) / (top_band - bottom_band)

    create_bb_chart(prices,rolling_std,top_band,bottom_band,sma,bbp)


if __name__ == "__main__":
    create_charts()
