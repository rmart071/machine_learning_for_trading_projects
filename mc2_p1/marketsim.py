"""MC2-P1: Market simulator."""

import pandas as pd
import numpy as np
import datetime as dt
import os
import math
from util import get_data, plot_data
import scipy.optimize as scp

'''
IGNORE ALL TRANSACTIONS
JUNE 15, 2011
'''


returns = 0
port_val = 0
alloc = []
rfr = 0.0
sf = 252.0
sv=1.0
the_big_day = '2011-06-15'
# prices = 0
# prices_SPY = 0

def retrieve_orders(orders_file = "./orders/orders.csv"):
    orders_dataframe = pd.read_csv(orders_file, index_col='Date', parse_dates=True, na_values=['nan'])
    orders_dataframe.loc[orders_dataframe['Order'] == 'SELL', 'Shares'] *= -1
    return orders_dataframe

def create_price_dataframe():
    pass

def create_trades_dataframe(orders_dataframe, price_dataframe):
    symbol_list = list(set(orders_dataframe['Symbol'].tolist()))

    data = {}
    for symbols in symbol_list:
        data.update({symbols:None})

    for symbols in data:
        locations = np.where(orders_dataframe['Symbol'] == symbols)
        data_array = np.zeros((len(orders_dataframe)))
        for num in locations[0]:
            data_array[num] = int(orders_dataframe[num:num+1]['Shares'])
        data.update({symbols:data_array})

    trades_dataframe = pd.DataFrame(data=data, index=orders_dataframe.index)

    empty_df = pd.DataFrame(data=None, index=price_dataframe.index)


    trades_dataframe = empty_df.join(trades_dataframe).fillna(0)

    trades_dataframe['Cash'] = (price_dataframe * trades_dataframe).sum(axis=1)

    return trades_dataframe


def create_holdings_dataframe(trades_dataframe, start_val):
    holdings_df = pd.DataFrame(data=0, index=trades_dataframe.index,
                               columns=trades_dataframe.columns)
    start_values = np.zeros(0)
    for index in trades_dataframe:
        share = np.zeros(0)
        share_count = 0.0
        if index != 'Cash':
            for row in trades_dataframe[index]:
                share_count += float(row)
                share = np.append(share, share_count)
            holdings_df[index] = share
        else:
            for row in trades_dataframe[index]:
                start_val -= float(row)
                start_values = np.append(start_values, start_val)
            holdings_df[index] = start_values
    holdings_df = holdings_df.groupby(holdings_df.index).first()
    return holdings_df

def calculate_values_dataframe(prices_dataframe, holdings_dataframe):
    return prices_dataframe * holdings_dataframe


def determine_leverage(values_dataframe, orders_dataframe):
    col_list = list(values_dataframe)
    col_list.remove('Cash')
    absolute_investments = abs(values_dataframe[col_list]).sum(axis=1)
    investments = values_dataframe[col_list].sum(axis=1);
    index_list = None

    leverage = absolute_investments / (investments + values_dataframe['Cash'])
    if leverage.any() > 0:
        test = np.where(leverage > 3)[0]
        index_list = list(values_dataframe.index[test])

    return index_list

def calculate_date_range(orders_dataframe):
    start_date = orders_dataframe.index.min()
    end_date = orders_dataframe.index.max()
    return start_date, end_date

def create_market_simulator_dataframes(symbols, orders_dataframe, start_val, start_date, end_date):

    prices_df = get_data(symbols, pd.date_range(start_date, end_date))
    prices_df['Cash'] = 1
    prices_df = prices_df.drop(['SPY'], axis=1)

    trades_df = create_trades_dataframe(orders_dataframe, prices_df)

    holdings_df = create_holdings_dataframe(trades_df, start_val)
    values_df = calculate_values_dataframe(prices_df, holdings_df)

    return values_df

def compute_portvals(orders_file = "./orders/orders.csv", start_val = 1000000):

    # read in the file
    orders_df = retrieve_orders(orders_file)

    row_loc = np.where(orders_df.index == the_big_day)[0]
    orders_df = orders_df.drop(orders_df.index[row_loc])

    start_date, end_date = calculate_date_range(orders_df)
    symbols = list(set(orders_df['Symbol'].tolist()))
    values_df = values_df = create_market_simulator_dataframes(symbols,orders_df,start_val,start_date, end_date)

    leverage = determine_leverage(values_df, orders_df)
    if len(leverage) > 0 :
        orders_df = orders_df.drop(orders_df.loc[leverage].index)
        values_df = create_market_simulator_dataframes(symbols,orders_df,start_val,start_date,end_date)

    portvals = values_df.sum(axis=1)
    return portvals

def compute_portfolio_stats(prices, allocs, rfr = 0.0, sf = 252.0):
    port_value = compute_daily_portfolio_values(prices, allocs)

    daily_returns = compute_daily_returns(port_value)

    daily_ret = daily_returns[1:]
    cr = (port_value[-1] / port_value[0]) - 1

    adr = daily_ret.mean()
    sddr = daily_ret.std()

    sr = np.sqrt(sf) * ((adr - rfr) / sddr)

    return np.array([cr, adr, sddr, sr])

def compute_daily_portfolio_values(prices, allocs, sv=1.0):
    normed = prices / prices.ix[0]
    alloced = normed * allocs
    pos_val = alloced * sv
    return pos_val.sum(axis=1)

def compute_daily_returns(df):
    daily_returns = df.copy()
    daily_returns[1:] = (df[1:] / df[:-1].values) - 1
    daily_returns.ix[0] = 0

    return daily_returns


def test_code():
    # global prices, prices_SPY
    # this is a helper function you can use to test your code
    # note that during autograding his function will not be called.
    # Define input parameters

    of = "./orders/orders-leverage-3.csv"
    sv = 1000000

    # Process orders
    portvals = compute_portvals(orders_file = of, start_val = sv)
    if isinstance(portvals, pd.DataFrame):
        portvals = portvals[portvals.columns[0]] # just get the first column
    else:
        "warning, code did not return a DataFrame"
    
    # Get portfolio stats
    # Here we just fake the data. you should use your code from previous assignments.

    start_date = dt.datetime(2011,1,10)
    end_date = dt.datetime(2011,12,20)

    syms = ['$SPX', 'SPY']
    SPX = ['$SPX']
    SPY = ['SPY']

    prices_all = get_data(syms, pd.date_range(start_date, end_date))  # automatically adds SPY
    prices_SPX = prices_all[SPX]  # only portfolio symbols
    #
    prices_SPY = prices_all[SPY]  # only SPY, for comparison later
    # prices_SPY.columns = ['SPY']
    alloc_SPX = np.asarray([1. / len(SPX)] * len(SPX), dtype=np.float64)
    alloc_SPY = np.asarray([1. / len(SPY)] * len(SPY), dtype=np.float64)
    cum_ret,\
    avg_daily_ret,\
    std_daily_ret,\
    sharpe_ratio = compute_portfolio_stats(prices_SPY,alloc_SPY)

    cum_ret_SPY,\
    avg_daily_ret_SPY,\
    std_daily_ret_SPY,\
    sharpe_ratio_SPY = compute_portfolio_stats(prices_SPX,alloc_SPX)


    # # Compare portfolio against $SPX
    print "Date Range: {} to {}".format(start_date, end_date)
    print
    print "Sharpe Ratio of Fund: {}".format(sharpe_ratio)
    print "Sharpe Ratio of $SPX : {}".format(sharpe_ratio_SPY)
    print
    print "Cumulative Return of Fund: {}".format(cum_ret)
    print "Cumulative Return of $SPX : {}".format(cum_ret_SPY)
    print
    print "Standard Deviation of Fund: {}".format(std_daily_ret)
    print "Standard Deviation of $SPX : {}".format(std_daily_ret_SPY)
    print
    print "Average Daily Return of Fund: {}".format(avg_daily_ret)
    print "Average Daily Return of $SPX : {}".format(avg_daily_ret_SPY)
    print
    print "Final Portfolio Value: {}".format(portvals[-1])

if __name__ == "__main__":
    test_code()
