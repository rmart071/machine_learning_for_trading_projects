"""MC1-P1: Analyze a portfolio."""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
from util import get_data, plot_data


def compute_daily_returns(df):
    daily_returns = df.copy()
    daily_returns[1:] = (df[1:] / df[:-1].values) - 1
    daily_returns.ix[0] = 0

    return daily_returns


def compute_portfolio_stats(prices, allocs, rfr, sf, port_value, daily_ret):
    daily_ret = daily_ret[1:]
    cr = (port_value[-1] / port_value[0]) - 1

    adr = daily_ret.mean()
    sddr = daily_ret.std()
    sr = np.sqrt(sf) * ((adr - rfr) / sddr)

    return cr, adr, sddr, sr


def compute_daily_portfolio_values(prices, allocs, sv):
    normed = prices / prices.ix[0]
    alloced = normed * allocs
    pos_val = alloced * sv
    return pos_val.sum(axis=1)


# This is the function that will be tested by the autograder
# The student must update this code to properly implement the functionality
def assess_portfolio(sd = dt.datetime(2008,1,1), ed = dt.datetime(2009,1,1), \
    syms = ['GOOG','AAPL','GLD','XOM'], \
    allocs=[0.1,0.2,0.3,0.4], \
    sv=1000000, rfr=0.0, sf=252.0, \
    gen_plot=False):

    # Read in adjusted closing prices for given symbols, date range
    dates = pd.date_range(sd, ed)
    prices_all = get_data(syms, dates)  # automatically adds SPY

    prices = prices_all[syms]  # only portfolio symbols
    prices_SPY = prices_all['SPY']  # only SPY, for comparison later

    # Get daily portfolio value
    port_val = compute_daily_portfolio_values(prices, allocs, sv)

    daily_ret = compute_daily_returns(port_val)


    # Get portfolio statistics (note: std_daily_ret = volatility)
    # cr, adr, sddr, sr = [0.25, 0.001, 0.0005, 2.1] # add code here to compute stats



    cr, adr, sddr, sr = \
    compute_portfolio_stats(prices=prices, allocs=[0.1, 0.2, 0.3, 0.4], rfr=0.0, sf=252.0, port_value=port_val, daily_ret=daily_ret)


    # Compare daily portfolio value with SPY using a normalized plot
    if gen_plot:
        # add code to plot here
        df_temp = pd.concat([port_val, prices_SPY], keys=['Portfolio', 'SPY'], axis=1)

        pass

    # Add code here to properly compute end value
    ev = sv * cr

    return cr, adr, sddr, sr, ev

def test_code():
    # This code WILL NOT be tested by the auto grader
    # It is only here to help you set up and test your code

    # Define input parameters
    # Note that ALL of these values will be set to different values by
    # the autograder!
    start_date = dt.datetime(2006,1,1)
    end_date = dt.datetime(2009,12,31)
    symbols = ['IBM']
    allocs = np.asarray([1. / len(symbols)] * len(symbols), dtype=np.float64)
    allocations = allocs
    start_val = 100000
    risk_free_rate = 0.0
    sample_freq = 252

    # Assess the portfolio
    cr, adr, sddr, sr, ev = assess_portfolio(sd = start_date, ed = end_date,\
        syms = symbols, \
        allocs = allocations,\
        sv = start_val, \
        gen_plot = False)

    # Print statistics
    print "Start Date:", start_date
    print "End Date:", end_date
    print "Symbols:", symbols
    print "Allocations:", allocations
    print "Sharpe Ratio:", sr
    print "Volatility (stdev of daily returns):", sddr
    print "Average Daily Return:", adr
    print "Cumulative Return:", cr

if __name__ == "__main__":
    test_code()
