"""
Test a learner.  (c) 2015 Tucker Balch
"""

import numpy as np
import math
import LinRegLearner as lrl
import RTLearner as rt
import BagLearner as bl
import sys
import time

if __name__=="__main__":
    if len(sys.argv) != 2:
        print "Usage: python testlearner.py <filename>"
        sys.exit(1)
    inf = open(sys.argv[1])
    data = np.array([map(float,s.strip().split(',')) for s in inf.readlines()])


    # compute how much of the data is training and testing
    train_rows = math.floor(0.6* data.shape[0])
    test_rows = data.shape[0] - train_rows

    # separate out training and testing data
    trainX = data[:train_rows,0:-1]
    trainY = data[:train_rows,-1]
    testX = data[train_rows:,0:-1]
    testY = data[train_rows:,-1]


    # def best4LinReg():
    #     X = np.random.normal(size=(100, 4))
    #     Y = 0.1 * X[:, 0] + 0.5 * np.sin(X[:, 1]) + 0.1 * np.random.normal(size=100)
    #     return X, Y



    choice = np.random.choice(data.shape[0], 2, replace=False)

    # create a learner and train it
    learner = lrl.LinRegLearner(verbose = True) # create a LinRegLearner
    learner.addEvidence(trainX, trainY) # train it
    learner2 = rt.RTLearner(leaf_size=25, verbose=False)
    learner2.addEvidence(trainX, trainY)
    Y2 = learner2.query(testX)

    start = time.time()
    learner3 = bl.BagLearner(rt.RTLearner, kwargs={"leaf_size":25}, bags=20, boost=False, verbose=False)
    learner3.addEvidence(trainX, trainY)
    end = time.time()
    print(end - start)
    Y = learner3.query(testX)

    start = time.time()
    # evaluate in sample
    predY = learner2.query(trainX) # get the predictions

    rmse = math.sqrt(((trainY - predY) ** 2).sum()/trainY.shape[0])
    print "In sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=trainY)
    print "corr: ", c[0,1]
    end = time.time()
    print(end - start)

    start = time.time()
    # evaluate out of sample
    predY = learner2.query(testX) # get the predictions

    rmse = math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0])
    print "Out of sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=testY)
    print "corr: ", c[0,1]
    end = time.time()
    print(end - start)
