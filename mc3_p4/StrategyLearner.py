"""
Template for implementing StrategyLearner  (c) 2016 Tucker Balch
"""

import datetime as dt
import numpy as np
import QLearner as ql
import pandas as pd
import util as ut
from datetime import timedelta

do_nothing=0
go_long = 1
go_short =2


def build_reward(action, current_value, future_value):
    if action == do_nothing:
        reward = 0

    if action == go_long:
        reward = compute_reward(current_value, future_value)

    if action == go_short:
        reward = compute_reward(current_value, future_value, inverse=True)


    return reward


def compute_reward(current_value, future_value, inverse=False):
    if inverse is True:
        return -(future_value - current_value) * 500

    return (future_value - current_value) * 500


def generate_order(action, holding):
    if holding == 0:
        if action == do_nothing:
            order = np.array([0])
            holding = 0
        elif action == go_long:
            order = np.array([500])
            holding = 1
        else:
            order = np.array([-500])
            holding = 2
    elif holding == 1:
        if action == do_nothing:
            order = np.array([-500])
            holding = 0
        elif action == go_long:
            order = np.array([0])
            holding = 1
        else:
            order = np.array([-1000])
            holding = 2
    else:
        if action == do_nothing:
            order = np.array([500])
            holding = 0
        elif action == go_long:
            order = np.array([1000])
            holding = 1
        else:
            order = np.array([0])
            holding = 2

    return order, holding


class StrategyLearner(object):

    # constructor
    def __init__(self, verbose = False):
        self.verbose = verbose

    def get_bollinger_bands(self, sma, rolling_std):
        top_band = sma + (2 * rolling_std)
        bottom_band = sma - (2 * rolling_std)

        return top_band.dropna(), bottom_band.dropna()

    def create_simple_moving_average(self, df, lookback):
        return pd.rolling_mean(df, window=lookback, min_periods=lookback)

    def create_rolling_std(self, df, lookback):
        return pd.rolling_std(df, window=lookback, min_periods=lookback)

    def create_sma_price_ratio(self, sma, price_df):
        return price_df / sma


    def discretize_indicators(self, prices):
        lookback =10
        sma = self.create_simple_moving_average(prices, 10)

        rolling_std = self.create_rolling_std(prices, lookback)
        top_band, bottom_band = self.get_bollinger_bands(sma, rolling_std)

        bbp = (prices - bottom_band) / (top_band - bottom_band)

        price_sma = self.create_sma_price_ratio(sma, prices)
        return bbp, price_sma



    # this method should create a QLearner, and train it for trading
    def addEvidence(self, symbol = "IBM", \
        sd=dt.datetime(2008,1,1), \
        ed=dt.datetime(2009,1,1), \
        sv = 10000):

        self.learner = ql.QLearner()
        # add your code to do learning here
        dates = pd.date_range(sd, ed);
        datesLookup = pd.date_range(sd - timedelta(days=14), ed)
        prices = ut.get_data([symbol], datesLookup, addSPY=False).dropna()

        bbp, price_sma = self.discretize_indicators(prices)


        prices = prices.ix[dates].dropna()

        # trades_dataframe = pd.DataFrame(index=prices.index, columns=[symbol])
        trades_dataframe = prices.copy()

        trades_dataframe[:] = 0.0


        first = True

        total_rewards = 1
        count = 0
        last_val = 0

        while last_val != total_rewards or count < 10:
            if count == 100:
                break
            total_rewards_1 = sv

            last_val = total_rewards

            total_rewards = sv
            test = pd.qcut(x=bbp[symbol].dropna(), q=10, labels=False)
            test2 = pd.qcut(x=price_sma[symbol].dropna(), q=10, labels=False)
            discretized_df = test.astype(str).str.cat(others=test2.astype(str))

            state = discretized_df[0]  # convert the location to a state

            action = self.learner.querysetstate(state)  # set the state and get first action
            current_holding = 0

            count += 1

            if first is True:

                for i in range(1, len(prices)-1):
                    reward = build_reward(action, prices.values[i][0],prices.values[i+1][0])
                    orders, current_holding = generate_order(action, current_holding)

                    action = self.learner.query(state, reward)

                    index_val = trades_dataframe.index.get_loc(prices.index[i])

                    trades_dataframe.ix[index_val][symbol] =orders[0]

                    state = discretized_df[i]
                    total_rewards_1 += reward


                # trades_dataframe.ix[len(prices)-1][symbol] = 0
                last_val = total_rewards_1

                first = False
            else:

                for i in range(1, len(prices) - 1):
                    reward = build_reward(action, prices.values[i][0], prices.values[i + 1][0])
                    orders, current_holding = generate_order(action, current_holding)

                    action = self.learner.query(state, reward)


                    index_val = trades_dataframe.index.get_loc(prices.index[i])

                    trades_dataframe.ix[index_val][symbol] = orders[0]

                    state = discretized_df[i]
                    total_rewards += reward
                # trades_dataframe.ix[len(prices) - 1][symbol] = 0



        #
        # # let's get the indicator values
        #
        #
        #
        # # example usage of the old backward compatible util function
        # syms=[symbol]
        # dates = pd.date_range(sd, ed)
        # prices_all = ut.get_data(syms, dates)  # automatically adds SPY
        # prices = prices_all[syms]  # only portfolio symbols
        # prices_SPY = prices_all['SPY']  # only SPY, for comparison later
        #
        #
        # # print(len(sma))
        # # step_size = len(sma)/10
        # # for i in range(0, step_size):
        # #     print(sma[i+1] * step_size)
        #
        # if self.verbose: print prices
        #
        # # example use with new colname
        # volume_all = ut.get_data(syms, dates, colname = "Volume")  # automatically adds SPY
        # volume = volume_all[syms]  # only portfolio symbols
        # volume_SPY = volume_all['SPY']  # only SPY, for comparison later
        # if self.verbose: print volume

    # this method should use the existing policy and test it against new data
    def testPolicy(self, symbol = "IBM", \
        sd=dt.datetime(2009,1,1), \
        ed=dt.datetime(2010,1,1), \
        sv = 10000):

        # here we build a fake set of trades
        # your code should return the same sort of data
        dates = pd.date_range(sd, ed)
        datesLookup = pd.date_range(sd - timedelta(days=14), ed)

        prices_all = ut.get_data([symbol], datesLookup).dropna()  # automatically adds SPY
        trades = prices_all[[symbol,]].dropna()  # only portfolio symbols

        bbp, price_sma = self.discretize_indicators(trades)

        trades = trades.ix[dates].dropna()

        # trades_dataframe = pd.DataFrame(index=trades.index, columns=[symbol])
        trades_dataframe = trades.copy()

        trades_dataframe[:] = 0.0

        test = pd.qcut(x=bbp[symbol].dropna(), q=10, labels=False)
        test2 = pd.qcut(x=price_sma[symbol].dropna(), q=10, labels=False)
        discretized_df = test.astype(str).str.cat(others=test2.astype(str))

        state = discretized_df[0]  # convert the location to a state

        action = self.learner.querysetstate(state)  # set the state and get first action
        current_holding = 0
        total_rewards =sv

        for i in range(1, len(trades) - 1):
            reward = build_reward(action, trades.values[i][0], trades.values[i + 1][0])
            orders, current_holding = generate_order(action, current_holding)
            action = self.learner.querysetstate(state)

            index_val = trades_dataframe.index.get_loc(trades.index[i])

            trades_dataframe.ix[index_val][symbol] = orders[0]

            state = discretized_df[i]
            total_rewards += reward

        # trades_dataframe.ix[len(trades) - 1][symbol] = 0

        print(trades_dataframe)

        return trades_dataframe

        # trades_SPY = prices_all['SPY']  # only SPY, for comparison later
        # trades.values[:,:] = 0 # set them all to nothing
        # trades.values[3,:] = 500 # add a BUY at the 4th date
        # trades.values[5,:] = -500 # add a SELL at the 6th date
        # trades.values[6,:] = -500 # add a SELL at the 7th date
        # trades.values[8,:] = 1000 # add a BUY at the 9th date
        # if self.verbose: print type(trades) # it better be a DataFrame!
        # if self.verbose: print trades
        # if self.verbose: print prices_all
        # print(trades)
        # return trades

if __name__=="__main__":
    print "One does not simply think up a strategy"
