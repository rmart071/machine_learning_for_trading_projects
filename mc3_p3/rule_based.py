import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import gridspec
import numpy as np
import datetime as dt
from util import get_data
from datetime import timedelta
from indicators import create_exponential_moving_average,create_rolling_std,\
                        create_simple_moving_average,create_sma_price_ratio,\
                        get_bollinger_bands,create_sma_price_ratio,create_macd


returns = 0
port_val = 0
alloc = []
rfr = 0.0
sf = 252.0
sv=1.0


def generate_chart(df, symbol):


    pass



def build_order(start_date='2006-01-01', end_date='2009-12-31'):
    # i think you can index for every 10 by doing df[::10]
    dates = pd.date_range(start_date, end_date)
    symbols = ['IBM']

    trading_day_hold = 10
    price = get_data(symbols, dates)

    sma = create_simple_moving_average(price, trading_day_hold)
    ema = create_exponential_moving_average(price, trading_day_hold)

    rolling_std = create_rolling_std(price, trading_day_hold)
    top_band, bottom_band = get_bollinger_bands(sma, rolling_std)

    bbp = (price - bottom_band) / (top_band - bottom_band)

    orders = price.copy()
    orders.ix[:, :] = np.NaN

    sma = create_sma_price_ratio(sma,price)

    ema = create_sma_price_ratio(ema, price)

    macd = create_macd(price, 26, 12)


    orders[(bbp < 0.5) & (sma < 0.99)] = 1
    orders[(bbp > 0.5) & (sma > 0.99)] = -1



    # Forward fill NaNs with previous values, then fill remaining NaNs with 0.
    orders.ffill(inplace=True)
    orders.fillna(0, inplace=True)

    # We now have a dataframe with our TARGET SHARES on every day, including holding periods.

    # Now take the diff, which will give us an order to place only when the target shares changed.
    orders[1:] = orders.diff()
    orders.ix[0] = 0

    # And more importantly, drop all rows with no non-zero values (i.e. no orders).
    orders = orders.loc[(orders != 0).any(axis=1)]

    order_list = []

    for day in orders.index:
        for sym in symbols:
            if orders.ix[day, sym] > 0:
                order_list.append([day.date(), sym, 'BUY', 500])
            elif orders.ix[day, sym] < 0:
                order_list.append([day.date(), sym, 'SELL', 500])


    orders2 = pd.DataFrame(data=order_list, columns=["Date","Symbol","Order","Shares"])

    orders2.to_csv('orders/orders1.csv', index=False)


def retrieve_orders(orders_file = "./orders/orders1.csv"):
    orders_dataframe = pd.read_csv(orders_file, index_col='Date', parse_dates=True, na_values=['nan'])
    orders_dataframe.loc[orders_dataframe['Order'] == 'SELL', 'Shares'] *= -1

    return orders_dataframe

def create_price_dataframe():
    pass

def create_trades_dataframe(orders_dataframe, price_dataframe):
    symbol_list = list(set(orders_dataframe['Symbol'].tolist()))

    data = {}
    for symbols in symbol_list:
        data.update({symbols:None})

    for symbols in data:
        locations = np.where(orders_dataframe['Symbol'] == symbols)
        data_array = np.zeros((len(orders_dataframe)))
        for num in locations[0]:
            data_array[num] = int(orders_dataframe[num:num+1]['Shares'])
        data.update({symbols:data_array})

    trades_dataframe = pd.DataFrame(data=data, index=orders_dataframe.index)
    last_date = price_dataframe.index[-1]
    last_shares = -trades_dataframe.ix[-1]['IBM']

    empty_df = pd.DataFrame(data=None, index=price_dataframe.index)


    trades_dataframe = empty_df.join(trades_dataframe).fillna(0)

    trades_shifted_by_10 = trades_dataframe[::10]

    df_ = pd.DataFrame(columns=trades_shifted_by_10.columns, index=trades_shifted_by_10.index)
    df_ = df_.fillna(0)

    columns = ['date', 'IBM']
    for i in range(0, len(trades_shifted_by_10.index)):
        if trades_shifted_by_10.values[i] == 500 :
            dict1 = [[trades_shifted_by_10.index[i], 500]]
            dict2 = [[trades_shifted_by_10.index[i+1], -500]]
            df = pd.DataFrame(dict2, columns=columns)
            df.set_index(['date'], inplace=True)
            del df.index.name
            df_ = df_.append(df, ignore_index=False)
            trades_dataframe = trades_dataframe.append(df, ignore_index=False)

        elif trades_shifted_by_10.values[i] == -500 :
            dict1 = [[trades_shifted_by_10.index[i], -500]]
            dict2 = [[trades_shifted_by_10.index[i+1], 500]]
            df = pd.DataFrame(dict2, columns=columns)
            df.set_index(['date'], inplace=True)

            del df.index.name
            df_ = df_.append(df, ignore_index=False)
            trades_dataframe = trades_dataframe.append(df, ignore_index=False)


    for index, row in trades_dataframe.iterrows():
        if index not in df_.index:
            trades_dataframe.loc[index] = 0

    dict2 = [[last_date, last_shares]]
    df = pd.DataFrame(dict2, columns=columns)
    df.set_index(['date'], inplace=True)
    del df.index.name
    trades_dataframe = trades_dataframe.append(df, ignore_index=False)

    trades_dataframe = trades_dataframe.sort_index()

    trades_dataframe['Cash'] = (price_dataframe * trades_dataframe).sum(axis=1)
    return trades_dataframe


def create_holdings_dataframe(trades_dataframe, start_val):
    holdings_df = pd.DataFrame(data=0, index=trades_dataframe.index,
                               columns=trades_dataframe.columns)
    start_values = np.zeros(0)

    for index in trades_dataframe:
        share = np.zeros(0)
        share_count = 0.0
        if index != 'Cash':
            for row in trades_dataframe[index]:
                share_count += float(row)
                share = np.append(share, share_count)
            holdings_df[index] = share
        else:
            for row in trades_dataframe[index]:
                start_val -= float(row)
                start_values = np.append(start_values, start_val)
            holdings_df[index] = start_values
    holdings_df = holdings_df.groupby(holdings_df.index).first()
    return holdings_df

def calculate_values_dataframe(prices_dataframe, holdings_dataframe):
    return prices_dataframe * holdings_dataframe


def determine_leverage(values_dataframe, orders_dataframe):
    col_list = list(values_dataframe)
    col_list.remove('Cash')
    absolute_investments = abs(values_dataframe[col_list]).sum(axis=1)
    investments = values_dataframe[col_list].sum(axis=1);
    index_list = None

    leverage = absolute_investments / (investments + values_dataframe['Cash'])
    if leverage.any() > 0:
        test = np.where(leverage > 3)[0]
        index_list = list(values_dataframe.index[test])

    return index_list

def calculate_date_range(orders_dataframe):
    start_date = orders_dataframe.index.min()
    end_date = orders_dataframe.index.max()
    return start_date, end_date

def create_market_simulator_dataframes(symbols, orders_dataframe, start_val, start_date, end_date):

    prices_df = get_data(symbols, pd.date_range(start_date, end_date))
    prices_df['Cash'] = 1
    prices_df = prices_df.drop(['SPY'], axis=1)

    trades_df = create_trades_dataframe(orders_dataframe, prices_df)

    holdings_df = create_holdings_dataframe(trades_df, start_val)
    values_df = calculate_values_dataframe(prices_df, holdings_df)
    return values_df,trades_df

def compute_portvals(orders_file, start_val = 100000, start_date='2006-01-01', end_date='2009-12-31'):
    # read in the file
    orders_df = retrieve_orders(orders_file)

    # start_date, end_date = calculate_date_range(orders_df)
    symbols = list(set(orders_df['Symbol'].tolist()))
    values_df, trades_df = create_market_simulator_dataframes(symbols,orders_df,start_val,start_date, end_date)

    leverage = determine_leverage(values_df, orders_df)
    if leverage is not None and len(leverage) > 0 :
        orders_df = orders_df.drop(orders_df.loc[leverage].index)
        values_df, trades_df = create_market_simulator_dataframes(symbols,orders_df,start_val,start_date,end_date)

    portvals = values_df.sum(axis=1)
    return portvals, trades_df, values_df

def compute_portfolio_stats(prices, allocs, rfr = 0.0, sf = 252.0):
    port_value = compute_daily_portfolio_values(prices, allocs)

    daily_returns = compute_daily_returns(port_value)

    daily_ret = daily_returns[1:]
    cr = (port_value[-1] / port_value[0]) - 1

    adr = daily_ret.mean()
    sddr = daily_ret.std()

    sr = np.sqrt(sf) * ((adr - rfr) / sddr)

    return np.array([cr, adr, sddr, sr])

def compute_daily_portfolio_values(prices, allocs, sv=1.0):
    normed = prices / prices.ix[0]
    alloced = normed * allocs
    pos_val = alloced * sv
    return pos_val.sum(axis=1)

def compute_daily_returns(df):
    daily_returns = df.copy()
    daily_returns[1:] = (df[1:] / df[:-1].values) - 1
    daily_returns.ix[0] = 0

    return daily_returns


def create_rule_based_chart(prices, portvals, trades_df, image, ml_portvals=None, ml_trades=None):
    plt.rc('axes', grid=True)
    plt.rc('grid', color='0.75', linestyle='-', linewidth=0.5)


    sell_dates = trades_df[trades_df['IBM'] > 1].index.tolist()
    buy_dates = trades_df[trades_df['IBM'] < -1].index.tolist()

    gs = gridspec.GridSpec(1, 1)

    fig = plt.figure()
    ax = plt.subplot(gs[0, :])

    prices.plot(title="Benchmarks", label='Historical Values', ax=ax, color='black')
    portvals.plot(label='Rule Based Portfolio', ax=ax, color='blue')

    if ml_portvals is not None:
        ml_portvals.plot(label='ML Based Portfolio', ax=ax, color='green')
        sell_dates = ml_trades[ml_trades['IBM'] > 1].index.tolist()
        buy_dates = ml_trades[ml_trades['IBM'] < -1].index.tolist()

    # Add axis labels and legend
    ax.set_xlabel("Date")
    ax.set_ylabel("Price")
    ax.legend(loc='upper left')

    for dates in sell_dates:
        ax.axvline(dates, linestyle="solid", linewidth=1, color='green')
    for dates in buy_dates:
        ax.axvline(dates, linestyle="solid", linewidth=1, color='red')
    for dates in buy_dates:
        if dates in sell_dates:
            ax.axvline(dates, linestyle="dashed", linewidth=1, color='black')
        if dates in buy_dates:
            ax.axvline(dates, linestyle="dashed", linewidth=1, color='black')


    plt.savefig(image)


def market_simulator(of=None, image=None, start_date='2006-01-01', end_date='2009-12-31'):
    # global prices, prices_SPY
    # this is a helper function you can use to test your code
    # note that during autograding his function will not be called.
    # Define input parameters
    rule_based = "orders/orders1.csv"
    if image is None:
        image = 'images/benchmarks.png'

    sv = 100000

    ml_portvals_all=None
    trades_df2 = None

    # Process orders
    portvals, trades_df, values_df = compute_portvals(orders_file=rule_based, start_val=sv, start_date=start_date, end_date=end_date)
    portvals = pd.DataFrame(portvals)
    portvals_all = portvals.copy()
    if isinstance(portvals_all, pd.DataFrame):
        portvals = portvals_all[portvals_all.columns[0]]  # just get the first column
    else:
        print "warning, code did not return a DataFrame"

    if of is not None:
        ml_portvals, trades_df2, values_df2 = compute_portvals(orders_file=of, start_val=sv, start_date=start_date, end_date=end_date)
        ml_portvals = pd.DataFrame(ml_portvals)
        ml_portvals.columns = ['IBM']
        ml_portvals_all = ml_portvals.copy()

    if isinstance(ml_portvals_all, pd.DataFrame):
        ml_portvals = ml_portvals_all[ml_portvals_all.columns]  # just get the first column
        df2 = pd.DataFrame(data=ml_portvals_all.values, columns=['ML-based Portfolio'], index=ml_portvals_all.index)
        ml_portvals_all = df2 / df2.ix[0]
        ml_portvals_all.ix[0] = 1

    else:
        "warning, code did not return a DataFrame"
    print(ml_portvals_all)

    # Get portfolio stats
    # Here we just fake the data. you should use your code from previous assignments.

    dates = pd.date_range(start_date, end_date)

    syms = ['IBM', 'SPY']
    IBM = ['IBM']

    prices_all = get_data(syms, dates)  # automatically adds SPY
    #
    prices_IBM = prices_all[IBM]  # only SPY, for comparison later
    # prices_SYS = prices_all[syms]

    alloc_IBM = np.asarray([1. / len(IBM)] * len(IBM), dtype=np.float64)
    cum_ret, \
    avg_daily_ret, \
    std_daily_ret, \
    sharpe_ratio = compute_portfolio_stats(prices_IBM, alloc_IBM)

    df = pd.DataFrame(data=portvals, columns=['Rule-based Portfolio'])

    alloc_orders = np.asarray([1. / len(IBM)] * len(IBM), dtype=np.float64)
    cum_ret_2, \
    avg_daily_ret_2, \
    std_daily_ret_2, \
    sharpe_ratio_2 = compute_portfolio_stats(df, alloc_orders)

    # df2 = pd.DataFrame(data=ml_portvals_all, columns=['ML-based Portfolio'])
    #
    # alloc_orders = np.asarray([1. / len(IBM)] * len(IBM), dtype=np.float64)
    #
    # cum_ret_3, \
    # avg_daily_ret_3, \
    # std_daily_ret_3, \
    # sharpe_ratio_3 = compute_portfolio_stats(df2, alloc_orders)



    prices_IBM = prices_IBM/prices_IBM.ix[0]
    portvals_all = df/df.ix[0]
    prices_IBM.ix[0] = 1
    portvals_all.ix[0] = 1

    if of is None:
        ml_portvals_all = None
        trades_df2 = None

    create_rule_based_chart(prices_IBM, portvals_all,trades_df, image, ml_portvals_all, trades_df2)

    print "Date Range: {} to {}".format(start_date, end_date)
    # print"Sharpe Ratio of ML: {}".format(sharpe_ratio_2)
    print"Sharpe Ratio of Fund: {}".format(sharpe_ratio_2)
    print"Sharpe Ratio of IBM: {}".format(sharpe_ratio)
    print
    # print"Cumulative Return of ML: {}".format(cum_ret_3)
    print"Cumulative Return of Fund: {}".format(cum_ret_2)
    print"Cumulative Return of IBM: {}".format(cum_ret)
    print
    # print"Standard Deviation of ML: {}".format(std_daily_ret_3)
    print"Standard Deviation of Fund: {}".format(std_daily_ret_2)
    print"Standard Deviation of IBM: {}".format(std_daily_ret)
    print
    # print"Average Daily of ML: {}".format(avg_daily_ret_3)
    print"Average Daily Return of Fund: {}".format(avg_daily_ret_2)
    print"Average Daily Return of IBM: {}".format(avg_daily_ret)

    print
    print"Final Portfolio Value: {}".format(portvals[-1])
    if of is not None:
        print "Final Portfolio ML Value: {}".format(ml_portvals.iloc[-1]['IBM'])




if __name__ == "__main__":
    build_order()
    market_simulator()