"""Adapted from MC2-P1: Market simulator."""

import pandas as pd
import numpy as np
import datetime as dt
import os
import math
from util import get_data, plot_data

def compute_portvals(orders, start_date, end_date, start_val = 1000000, syms='IBM'):

    ignoredDates = []

    prices = get_data(syms, pd.date_range(start_date, end_date))
    prices['Cash']=1

    syms.append('Cash')

    trades = prices.copy()
    trades[trades >= 0] = 0
    trades[trades < 0] = 0


    holdings = prices.copy()
    for sym in syms:
        holdings[sym] = 0
    holdings.loc[holdings.index.min(),'Cash'] = start_val
    holdings['Value']=0

    import_orders(ignoredDates, orders, prices, trades)

    compute_holdings(holdings, prices, syms, trades, ignoredDates)

    return holdings['Value']


def compute_holdings(holdings, prices, syms, trades, ignoredDates):

    for index in holdings.index:
        if index.date() != holdings.index.min().date():
            holdings.loc[index] = holdings.iloc[holdings.index.get_loc(index) - 1]

        value = 0
        absoluteValue = 0
        for sym in syms:
            holdings.loc[index, sym] += trades.loc[index, sym]

            value += holdings.loc[index, sym] * prices.loc[index, sym]
            if sym != "Cash":
                absoluteValue += abs(holdings.loc[index, sym] * prices.loc[index, sym])

        holdings.loc[index, 'Value'] = value

def import_orders(ignoredDates, orders, prices, trades):
    for row in orders:
        symbol = row[1]
        shares = row[3]
        date = pd.to_datetime(row[0])

        if date not in ignoredDates:
            if row[2] == "BUY":
                trades.loc[date, symbol] += shares
                trades.loc[date, 'Cash'] += - shares * prices.loc[date, symbol]
            else:
                trades.loc[date, symbol] += -shares
                trades.loc[date, 'Cash'] += shares * prices.loc[date, symbol]