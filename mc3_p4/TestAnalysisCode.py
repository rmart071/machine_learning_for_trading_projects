import time
import unittest
import StrategyLearner as sl
import datetime as dt
import marketSimulation as sim
import pandas as pd
import util as ut

testStart = dt.datetime(2010,1,1)
testEnd =dt.datetime(2010,12,31)
trainStart =dt.datetime(2006,1,1)
trainEnd =dt.datetime(2009,12,31)
ml4tSym = "ML4T-220"
ibmSym = "IBM"

class TestAnalysisCode(unittest.TestCase):

    #For ML4T-220, the trained policy should provide a cumulative return greater than 400% in sample (33 points)
    def test_ML4T220InSample(self):

        syms=[ml4tSym]
        dates = pd.date_range(trainStart, trainEnd)
        prices_all = ut.get_data(syms, dates)
        prices = prices_all[syms]
        marketReturn = prices.div(prices.iloc[0])

        learner = sl.StrategyLearner(verbose = False) # constructor
        learner.addEvidence(symbol = ml4tSym, sd=trainStart, ed=trainEnd, sv = 100000) # training phase
        df_trades = learner.testPolicy(symbol = ml4tSym, sd=trainStart, ed=trainEnd, sv = 100000) # testing phase
        portVals = sim.compute_portvals(df_trades, trainStart, trainEnd, 100000, syms)
        tradingReturn = portVals.div(portVals.iloc[0])

        self.assertGreater(tradingReturn, 4, "The ML4T-220 In Sample trading return was not at least 400% of the market return")

    #For ML4T-220, the trained policy should provide a cumulative return greater than 100% out of sample (33 points)
    def test_ML4T220OutSample(self):

        syms=[ml4tSym]
        dates = pd.date_range(testStart, testEnd)
        prices_all = ut.get_data(syms, dates)
        prices = prices_all[syms]
        marketReturn = prices.div(prices.iloc[0])

        learner = sl.StrategyLearner(verbose = False) # constructor
        learner.addEvidence(symbol = ml4tSym, sd=trainStart, ed=trainEnd, sv = 100000) # training phase
        df_trades = learner.testPolicy(symbol = ml4tSym, sd=testStart, ed=testEnd, sv = 100000) # testing phase
        portVals = sim.compute_portvals(df_trades, testStart, testEnd, 100000, syms)
        tradingReturn = portVals.div(portVals.iloc[0])

        self.assertGreater((tradingReturn), 1, "The ML4T-220 Out of Sample trading return was not at least 100% of the market return")

    #For IBM, the trained policy should significantly outperform the benchmark in sample (34 points)
    def test_IBMInSample(self):

        syms=[ibmSym]
        dates = pd.date_range(trainStart, trainEnd)
        prices_all = ut.get_data(syms, dates)
        prices = prices_all[syms]
        marketReturn = prices.div(prices.iloc[0])

        learner = sl.StrategyLearner(verbose = False) # constructor
        learner.addEvidence(symbol = ibmSym, sd=trainStart, ed=trainEnd, sv = 100000) # training phase
        df_trades = learner.testPolicy(symbol = ml4tSym, sd=trainStart, ed=trainEnd, sv = 100000) # testing phase
        portVals = sim.compute_portvals(df_trades, trainStart, trainEnd, 100000, syms)
        tradingReturn = portVals.div(portVals.iloc[0])

        self.assertGreater((tradingReturn/marketReturn), 2, "The IBM In Sample trading return was not at least 200% of the market return")


if __name__ == "__main__":
    unittest.main()