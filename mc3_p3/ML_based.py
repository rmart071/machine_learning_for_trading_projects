"""
Test a learner.  (c) 2015 Tucker Balch
"""

import numpy as np
import math
import sys
from rule_based import market_simulator,build_order
import RTLearner as rt
import BagLearner as bl
import pandas as pd
from util import get_data, plot_data
from indicators import create_exponential_moving_average,create_rolling_std,\
                        create_simple_moving_average,create_sma_price_ratio,\
                        get_bollinger_bands,create_sma_price_ratio,create_macd,normalize_dataframe


in_sample_start='2006-01-01'
in_sample_end='2009-12-31'
out_sample_start='2010-01-01'
out_sample_end='2010-12-31'



def build_data_array(sample_start='2006-01-01', sample_end='2009-12-31'):
    symbols = ['IBM']

    prices_df = get_data(symbols, pd.date_range(sample_start, sample_end))
    lookback = 10

    std_dev = pd.rolling_std(prices_df[symbols], window=lookback, min_periods=lookback)

    sma = create_simple_moving_average(prices_df[symbols], lookback)
    macd = create_macd(prices_df[symbols], 10, 10)

    bb_value = pd.DataFrame(columns=prices_df[symbols].columns, index=prices_df[symbols].index)
    ret = pd.DataFrame(columns=prices_df[symbols].columns, index=prices_df[symbols].index)

    for i in range(0, len(prices_df[symbols].index)):
        bb_value.ix[i] = (prices_df[symbols].values[i] - sma.values[i]) / (2 * std_dev.values[i])


    # put 9 instead of 10 to be inclusive
    for i in range(0, len(prices_df[symbols].index) - 9):
        ret.ix[i] = (prices_df[symbols].values[i + 9] / prices_df[symbols].values[i]) - 1.0

    sma = create_sma_price_ratio(sma, prices_df[symbols])
    macd = macd.dropna()

    bb_value = bb_value.dropna()
    sma = sma.dropna()

    bbpMatrix = bb_value.as_matrix()
    smaMatrix = sma.as_matrix()
    macdMatrix = macd.as_matrix()


    YBUY = 0.015
    YSELL = -0.015

    ret = ret.dropna()
    Y = pd.DataFrame(columns=ret.columns, index=ret.index)

    for i in range(0, len(ret.index)):
        if ret.values[i] > YBUY:
            Y.ix[i] = 1.0
        elif ret.values[i] < YSELL:
            Y.ix[i] = -1.0
        else:
            Y.ix[i] = 0.0

    order_list = []

    for day in Y.index:
        for sym in symbols:

            if Y.ix[day, sym] > 0:
                order_list.append([day.date(), sym, 'BUY', 500])
            elif Y.ix[day, sym] < 0:
                order_list.append([day.date(), sym, 'SELL', 500])


    orders2 = pd.DataFrame(data=order_list, columns=["Date", "Symbol", "Order", "Shares"])

    orders2.to_csv('orders/ml_orders.csv', index=False)

    Y = Y.as_matrix()

    numpy_array = np.hstack((bbpMatrix, smaMatrix, macdMatrix, Y))
    return numpy_array


if __name__=="__main__":
    data = build_data_array(in_sample_start, in_sample_end)

    # compute how much of the data is training and testing
    train_rows = math.floor(0.6* data.shape[0])
    test_rows = data.shape[0] - train_rows

    # separate out training and testing data
    trainX = data[:train_rows,0:-1]
    trainY = data[:train_rows,-1]
    testX = data[train_rows:,0:-1]
    testY = data[train_rows:,-1]


    choice = np.random.choice(data.shape[0], 2, replace=False)

    # create a learner and train it
    learner2 = rt.RTLearner(leaf_size=5, verbose=False)
    learner2.addEvidence(trainX, trainY)
    Y2 = learner2.query(testX)

    learner3 = bl.BagLearner(rt.RTLearner, kwargs={"leaf_size":5}, bags=20, boost=False, verbose=False)
    learner3.addEvidence(trainX, trainY)
    Y = learner3.query(testX)

    # evaluate in sample
    predY = learner2.query(trainX) # get the predictions

    rmse = math.sqrt(((trainY - predY) ** 2).sum()/trainY.shape[0])
    print "In sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=trainY)
    print "corr: ", c[0,1]

    # evaluate out of sample
    predY = learner2.query(testX) # get the predictions

    rmse = math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0])
    print "Out of sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=testY)
    print "corr: ", c[0,1]

    build_order(start_date=in_sample_start, end_date=in_sample_end)
    market_simulator(of='orders/ml_orders.csv', image='images/ml_benchmarks', start_date=in_sample_start, end_date=in_sample_end)
