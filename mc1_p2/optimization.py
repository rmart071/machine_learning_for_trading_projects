"""MC1-P2: Optimize a portfolio."""

import pandas as pd
import matplotlib
matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
from util import get_data, plot_data
import scipy.optimize as scp


returns = 0
port_val = 0
alloc = []
rfr = 0.0
sf = 252.0
sv=1.0
prices = 0
prices_SPY = []


def compute_daily_returns(df):
    daily_returns = df.copy()
    daily_returns[1:] = (df[1:] / df[:-1].values) - 1
    daily_returns.ix[0] = 0

    return daily_returns



def compute_portfolio_stats(prices, allocs, rfr, sf):

    port_value = compute_daily_portfolio_values(prices, allocs, sv)

    daily_returns = compute_daily_returns(port_value)

    daily_ret = daily_returns[1:]
    cr = (port_value[-1] / port_value[0]) - 1

    adr = daily_ret.mean()
    sddr = daily_ret.std()

    sr = np.sqrt(sf) * ((adr - rfr) / sddr)

    return np.array([cr, adr, sddr, sr])


def statistics(weights):
    daily_ret = returns[1:]

    pret = daily_ret.mean()
    pvol = daily_ret.std()
    return np.array([pret, pvol, pret/pvol])


def create_plot(start_date=dt.datetime(2008,1,1), end_date=dt.datetime(2009,12,31), syms=['IBM', 'X', 'HNZ', 'XOM', 'GLD']):

    dates = pd.date_range(start_date, end_date)
    prices_all = get_data(syms, dates)  # automatically adds SPY
    global prices, prices_SPY
    prices = prices_all[syms]  # only portfolio symbols
    prices_SPY = prices_all['SPY']  # only SPY, for comparison later

    spy_prices = prices_SPY / prices_SPY.ix[0]

    allocs = np.asarray([1. / len(syms)] * len(syms), dtype=np.float64)

    port_val = compute_daily_portfolio_values(prices, allocs, sv)

    df_temp = pd.concat([port_val, spy_prices], keys=['Portfolio', 'SPY'], axis=1)

    ax = df_temp.plot(title='Daily Portfolio Value and SPY', fontsize=12)
    ax.set_xlabel('Dates')
    ax.set_ylabel('Price')
    plt.savefig('comparison_optimal.png')



def compute_daily_portfolio_values(prices, allocs, sv):
    normed = prices / prices.ix[0]
    alloced = normed * allocs
    pos_val = alloced * sv
    return pos_val.sum(axis=1)


def compute_sharpe_ratio(weights):
    return -compute_portfolio_stats(prices,weights, rfr, sf)[3]

# This is the function that will be tested by the autograder
# The student must update this code to properly implement the functionality
def optimize_portfolio(sd=dt.datetime(2008,1,1), ed=dt.datetime(2009,1,1), \
    syms=['GOOG','AAPL','GLD','XOM'], gen_plot=False):

    # Read in adjusted closing prices for given symbols, date range
    dates = pd.date_range(sd, ed)
    prices_all = get_data(syms, dates)  # automatically adds SPY
    global prices, prices_SPY
    prices = prices_all[syms]  # only portfolio symbols
    prices_SPY = prices_all['SPY']  # only SPY, for comparison later


    global alloc
    alloc = np.asarray([1. / len(syms)] * len(syms), dtype=np.float64)

    cr, adr, sddr, sr = \
        compute_portfolio_stats(prices = prices,
                                allocs=alloc,
                                rfr = 0.0, sf = 252.0)

    noa = len(syms)
    x0 = (noa * [1. / noa])

    cons = ({'type':'eq', 'fun': lambda x: np.sum(x) - 1})
    bnds = tuple((0,1) for x in range(noa))

    min_result = scp.minimize(compute_sharpe_ratio, x0, method='SLSQP', bounds=bnds,constraints=cons)
    alloc = min_result.x
    sr =  min_result.fun*-1

    # Get daily portfolio value
    port_val = compute_daily_portfolio_values(prices, alloc, sv)


    # Compare daily portfolio value with SPY using a normalized plot
    if gen_plot:
        create_plot()

    return alloc, cr, adr, sddr, sr


def test_code():
    # This function WILL NOT be called by the auto grader
    # Do not assume that any variables defined here are available to your function/code
    # It is only here to help you set up and test your code

    # Define input parameters
    # Note that ALL of these values will be set to different values by
    # the autograder!


    start_date = dt.datetime(2009,1,1)
    end_date = dt.datetime(2010,1,1)
    symbols = ['GOOG', 'AAPL', 'GLD', 'XOM', 'IBM']

    # Assess the portfolio
    allocations, cr, adr, sddr, sr = optimize_portfolio(sd = start_date, ed = end_date,\
        syms = symbols, \
        gen_plot = True)



    # Print statistics
    print "Start Date:", start_date
    print "End Date:", end_date
    print "Symbols:", symbols
    print "Optimal Allocations:", allocations
    print "Sharpe Ratio:", sr
    print "Volatility (stdev of daily returns):", sddr
    print "Average Daily Return:", adr
    print "Cumulative Return:", cr




if __name__ == "__main__":
    # This code WILL NOT be called by the auto grader
    # Do not assume that it will be called
    test_code()
"""MC1-P2: Optimize a portfolio."""

