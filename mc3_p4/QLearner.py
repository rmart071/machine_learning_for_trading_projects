"""
Template for implementing QLearner  (c) 2015 Tucker Balch

Actions = 0 (do nothing), 1 (Go long), 2 (Go Short)
"""

import numpy as np
import random as rand

class QLearner(object):

    def __init__(self, \
        num_states=100, \
        num_actions = 3, \
        alpha = 0.2, \
        gamma = 0.9, \
        rar = 0.99, \
        radr = 0.999, \
        dyna = 0, \
        verbose = False):

        self.verbose = verbose
        self.num_states = num_states
        self.num_actions = num_actions
        self.random_action_rate = rar
        self.random_action_decay_rate = radr
        self.alpha = alpha
        self.gamma = gamma
        self.dyna = dyna
        self.s = 0
        self.a = 0

        # intialize Q table
        # self.Q = np.zeros((num_states, num_actions))
        self.Q = np.random.uniform(-0.001,0.001,[num_states,num_actions])
        self.R = np.zeros((num_states, num_actions))
        self.T = np.zeros((num_states, num_actions,num_states))

        self.Tc = np.zeros((num_states, num_actions,num_states))
        self.Tc.fill(0.00001)


    def querysetstate(self, s):
        """
        @summary: Update the state without updating the Q-table
        @param s: The new state
        @returns: The selected action
        """
        self.s = s
        if rand.uniform(0.0, 1.0) < self.random_action_rate:
            action  = rand.randint(0, self.num_actions - 1.0)
        else:
            action = self.find_max_value_for_state(self.s)
        if self.verbose: print "s =", s,"a =",action
        self.a = action

        return action

    def query(self,s_prime,r):
        """
        @summary: Update the Q table and return an action
        @param s_prime: The new state
        @param r: The new reward
        @returns: The selected action
        """


        if rand.uniform(0.0, 1.0) < self.random_action_rate:
            action = rand.randint(0, self.num_actions - 1.0)
        else:
            action = np.argmax(self.Q[s_prime, :])

        self.Q[self.s, self.a] = self.compute_update_rule(self.s, self.a, s_prime, r)


        if self.verbose: print "s =", s_prime,"a =",action,"r =",r

        self.random_action_rate *= self.random_action_decay_rate

        self.s = s_prime
        self.a = action

        return action

    def find_max_value_for_state(self, state):
        return np.argmax(self.Q[state, :])

    def compute_update_rule(self,state, action,  s_prime, reward):
        old_value = (1 - self.alpha) * self.Q[state, action]
        improved_estimate = reward + self.gamma * self.Q[s_prime, np.argmax(self.Q[s_prime, :])]

        new_value = old_value + self.alpha * improved_estimate
        return new_value

if __name__=="__main__":
    print "Remember Q from Star Trek? Well, this isn't him"
