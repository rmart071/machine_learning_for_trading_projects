# Machine Learning for Trading #

These are the assignment/projects completed for the course

### Assignments ###

* mc1_p1: Assess portfolio
* mc1_p2: Optimize a portfolio
* mc2_p1: Build a Market Simulator
* mc3_h1: Generate datasets that defeat learners
* mc3_p1: Implement and assess a regression learning using decision trees and random forest
* mc3_p2: Q-Learning maze navigation
* mc3_p3: Implement your own "manual" quant strategy, then do it with decision tree classification, compare
* mc3_p4: Q-Learning trader